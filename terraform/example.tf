provider "aws" {
  profile    = "default"
  region     = "eu-west-1"
}

resource "aws_instance" "example" {    
  ami           = "ami-04facb3ed127a2eb6"
  instance_type = "t2.micro"
  subnet_id     = "subnet-05457e1c5b6cfdeee"
}